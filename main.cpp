#include "rtcvImage.h"

//test

int main(int argc, char* argv[]){
    rtcvImage img1;
    img1.resize( 100, 100 );
    rtcvImage img2;
    img2 = img1;
    printf("Zuweisungsoperator: sx = %d sy = %d\r\n", img2.getSx(), img2.getSy() );

    rtcvImage img3(img1);
    printf("Kopierkonstruktor: sx = %d sy = %d\r\n", img3.getSx(), img3.getSy() );

    rtcvImage img4;
    img4.resize(100,100);
    rtcvImage img5;
    img5 = img4;

    img1 = img1;
    printf("self assignment: sx = %d sy = %d\r\n", img3.getSx(), img3.getSy() );

    unsigned char tmp = '+';
    rtcvImage img6;
    img6.resize(10,10);
    img6.set(2, 3, tmp);
    img6.output();

    unsigned char * imageData = new unsigned char[512 * 512];
    rtcvImage img7;
    //img7.resize(512, 512);
    bool test = img7.readPGM("lena.pgm", &imageData, 512, 512);
    cout << "bool test readPGM: " << test << endl;
    img7.setData(imageData, 512, 512);
    //img.output();
    delete[] imageData;
    printf("sx = %d sy = %d\r\n", img7.getSx(), img7.getSy() );

    //img7.drawLine(1,100, 100, 200);
    //img7.fill(255);

    rtcvImage img8;
    unsigned char * imageData1 = new unsigned char[512 * 512];
    img8.readPGM("lena.pgm", &imageData1, 512, 512);
    img8.setData(imageData1, 512, 512);
    img8.rotate(90);
    img8.writePGM("output1.pgm", img8.getDataPointer(), 512, 512);


    img7.drawLine(100,10,3,50, RIGHT);
    img7.drawLine(100,30,3,50, LEFT);
    img7.fillRect(100,50,1,1,255);

    img7.drawLine(280,90,3,50, DOWN);
    img7.drawLine(300,90,3,50, UP);
    img7.fillRect(320,90,1,1,255);



    //img7.fill(255);
    //img7.rotate(10);

    img7.drawRect(100,150,220,120);
    img7.fillRect(100,350,220,120,255);
    img7.writePGM("output.pgm", img7.getDataPointer(), 512, 512);


    img1.release();
    img2.release();
    img3.release();
    img4.release();
    img5.release();
    img6.release();
    img7.release();
    img8.release();
    
}
