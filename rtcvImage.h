//
// Created by Emanuel on 09.03.2017.
//

#ifndef T4_BILDBEARBEITUNG_RTCVIMAGE_H
#define T4_BILDBEARBEITUNG_RTCVIMAGE_H

#include <iostream>
#include <cstring>

#pragma warning(disable : 4996)
using namespace std;

enum Direction{UP, DOWN, LEFT, RIGHT};

class rtcvImage{
public:

    rtcvImage();
    ~rtcvImage();
    rtcvImage(const rtcvImage& rhs );


    rtcvImage& operator=(const rtcvImage & rhs);

    void resize(unsigned int sx, unsigned int sy);
    void release();

    unsigned int getSx() const;
    unsigned int getSy() const;

    bool writePGM(const char *fileName, const unsigned char *pData,
                  unsigned int sx, unsigned int sy);

    bool readPGM(const char * fileName, unsigned char ** pData,
                 unsigned int sx, unsigned int sy);
    
    // Extending the Class
    void output();

    void setData(const unsigned char* data, const unsigned int width, const unsigned int height);
    
    unsigned char* getDataPointer();

    void fill(const unsigned char color);
    void set(const size_t x, const size_t y, unsigned char color);

    void drawLine(unsigned int x_start, unsigned int y_start, const unsigned int lineWidth, const unsigned int length, const Direction direction );
    
    void drawRect(unsigned int pos_x, unsigned int pos_y, unsigned int width, unsigned int length);
    void fillRect(unsigned int pos_x, unsigned int pos_y, const unsigned int width, const unsigned int length, const unsigned char color);
    void rotate(int a);

private:
    unsigned int m_sx; // Bildbreite
    unsigned int m_sy; // Bildhöhe
    unsigned char* mp_data; // Bilddaten (8 Bit Helligkeit)
};


#endif //T4_BILDBEARBEITUNG_RTCVIMAGE_H
