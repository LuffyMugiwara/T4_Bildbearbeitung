
#include "Matrix.h"

typedef Matrix::data_t data_t;

using namespace std;

Matrix::Matrix(size_t rows, size_t cols, double val)
        :rows_m(rows),
         cols_m(cols)
{
    data_m = data_t(rows);
    for(size_t i=0; i < data_m.size(); i++)
    {
        data_m[i] = std::vector<double>(cols);

        for(size_t j=0; j < data_m[i].size(); j++)
            data_m[i][j] = val;
    }

}

void Matrix::outputMatrix()
{
    for(size_t i = 0; i < data_m.size(); i++)
    {
        for(size_t j = 0; j < data_m[i].size(); j++)
            cout << data_m[i][j] << " ";
        cout << endl;
    }
    cout << endl;
}







//need reference, so I can write on this element
double& Matrix::operator() (const size_t row_a, const size_t col_a)
{
    // indices need to be in bounds.
    if( !(row_a < rows_m && col_a < cols_m) )
    {
        cout << "Error!" <<endl;
        exit(-1);
    }
    // Return the specified element, retreived by 1D index.
    return  data_m[row_a][col_a];
}

const double& Matrix::operator() (const size_t row_a, const size_t col_a) const
{
    // indices need to be in bounds.
    if( !(row_a < rows_m && col_a < cols_m) )
    {
        cout << "Error!" <<endl;
        exit(-1);
    }
    // Return the specified element, retreived by 1D index.
    return  data_m[row_a][col_a];
}


size_t Matrix::rows()
{
    return rows_m;
}

size_t Matrix::cols()
{
    return cols_m;
}
