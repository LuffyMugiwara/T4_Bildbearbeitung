#include "rtcvImage.h"
#include "Matrix.h"
#include <math.h>
#include <cmath>


rtcvImage::rtcvImage()
 : m_sx(0),
   m_sy(0),
   mp_data(0)
{

}

rtcvImage::~rtcvImage()
{
    delete [] mp_data;
}

rtcvImage::rtcvImage(const rtcvImage& rhs )
 : mp_data(0)
{
    *this = rhs;
}


bool rtcvImage::writePGM(const char *fileName, const unsigned char *pData, unsigned int sx, unsigned int sy)
{
    FILE* fp = fopen(fileName, "wb");
    if(!fp)
        return false;
    fprintf(fp, "P5\n%d %d\n255\n", sx, sy);
    if(1 != fwrite(pData, sx*sy, 1, fp))
    {
		fclose(fp);
		return false;
    }
    fclose(fp);

    return true;
}

bool rtcvImage::readPGM(const char * fileName, unsigned char ** pData, unsigned int sx, unsigned int sy)
{
    FILE* fp = fopen(fileName, "rb");
    if (!fp)
        return false;
    char* commentBuffer = new char[100];
    fgets(commentBuffer, 100, fp);
    fgets(commentBuffer, 100, fp);
    fgets(commentBuffer, 100, fp);
    delete[] commentBuffer;
    delete[] *pData;
    *pData = new unsigned char[sx*sy];
    fread(*pData, sx*sizeof(unsigned char), sy, fp);
    fclose(fp);

    return true;
}


unsigned int rtcvImage::getSx() const
{
	return m_sx;
}
unsigned int rtcvImage::getSy() const
{
	return m_sy;
}

void rtcvImage::release()
{
    delete[] mp_data;
    mp_data = 0;
    m_sx = 0;
    m_sy = 0;
}

void rtcvImage::resize(unsigned int sx, unsigned int sy)
{
	m_sx = sx;
    m_sy = sy;
    delete [] mp_data;
    mp_data = new unsigned char[m_sx*m_sy];
}

rtcvImage& rtcvImage::operator=(const rtcvImage & rhs)
{
    if(this != &rhs)
    {
		m_sx = rhs.m_sx;
		m_sy = rhs.m_sy;
		delete[] mp_data;
		mp_data = new unsigned char[m_sx*m_sy];
		memcpy( mp_data, rhs.mp_data, m_sx*m_sy* sizeof(unsigned char) );
    }
    return *this;
}

// Aufgabe 1 - Erweiterung der Klasse
void rtcvImage::output()
{
    size_t len = m_sx * m_sy;
    for(size_t i=0; i<len; i++)
    {
		if(!(i%m_sx))
			cout << mp_data[i] << " " << endl;
		else
			cout << mp_data[i] << " ";
    }
    cout << endl;
}

void rtcvImage::setData(const unsigned char* data, const unsigned int width,const unsigned int height)
{
    if((height > m_sy) || (height > m_sx))
        resize(width, height);

    for(size_t i=0; i<width; i++)
    {
        for(size_t j=0; j<height; j++)
        {
            if(data[j*m_sx+i] > 0 && data[j*m_sx+i] < 255)
                set(i, j, data[j*m_sx+i]);	 // can rotate here
            else
            {
                set(i, j, 0);
                cout << "Warning: You tried to insert non-valid PGM values." << endl;
            }
        }
    }
}

unsigned char* rtcvImage::getDataPointer()
{
        return mp_data;
}

void rtcvImage::fill(const unsigned char color)
{
    for(size_t i=1; i<m_sx; i++)
    {
		for(size_t j=1; j<m_sy; j++)
			set(i, j, color);
    }
}

void rtcvImage::set(const size_t x, const size_t y, unsigned char color)
{
    if(x > 0 && y > 0)
    {
		if((x <= m_sx) && (y <= m_sy))
			mp_data[x+m_sx*y] = color;
    }
}


void rtcvImage::drawRect(unsigned int pos_x, unsigned int pos_y, unsigned int width, unsigned int length)
{
        unsigned char color = 255;
        unsigned int borderWidth = 5;

//        if(width+pos_x > m_sx)
//            width = m_sx;
//        if(length+pos_y > m_sy)
//            length = m_sy;


    drawLine(pos_x, pos_y, borderWidth, width, RIGHT);
    drawLine(pos_x, pos_y, borderWidth, length, DOWN);
    drawLine(pos_x + width, pos_y, borderWidth, length, DOWN);
    drawLine(pos_x , pos_y + length, borderWidth, width + borderWidth, RIGHT);

}

void rtcvImage::fillRect(unsigned int pos_x, unsigned int pos_y, const unsigned int width, const unsigned int length, const unsigned char color)
{
//    if(breite+pos_x > m_sx)
//        breite = m_sx;
//    if(lange+pos_y > m_sy)
//        lange = m_sy;

    for(size_t i=1; i<=width; i++)
        for(size_t j=1; j<=length; j++)
            set(i+pos_x, j+pos_y, color);
}

void rtcvImage::rotate(int a){
	unsigned char tmp_data[m_sx*m_sy];
	unsigned char x_coor;
	unsigned char y_coor;
	
	for(unsigned i = 0; i < m_sy; ++i){
		for(unsigned j = 0; j < m_sx; ++j){
			signed rx = (signed)((double)j*cos(a) - (double)i*sin(a));
			signed ry = (signed)((double)j*sin(a) + (double)i*cos(a));
			if(rx < 0 || ry < 0 || rx >= m_sx || ry >= m_sy)
				continue;
			tmp_data[i*m_sx + j] = mp_data[ry*m_sx+rx];
		}
	}
	
	size_t len = m_sx*m_sy;
	for(size_t i=0; i<len; i++){
		mp_data[i] = tmp_data[i];
	}
}


void rtcvImage::drawLine(unsigned int x_start, unsigned int y_start, const unsigned int lineWidth, const unsigned int length, const Direction direction )
{
    unsigned char color = 255;

    /*
     * Check if given start, end gets out of image
     * if so adjust it such that it fits into the image.
     */
//    if(x_start < 1)
//        x_start = 1;
//    if(y_start < 1)
//        y_start = 1;


    if(direction == RIGHT)
        fillRect(x_start, y_start, length, lineWidth, color);

    if(direction == LEFT)
        fillRect(x_start - length - 1,y_start, length, lineWidth, color);

    if(direction == DOWN)
        fillRect(x_start, y_start, lineWidth, length, color);

    if(direction == UP)
        fillRect(x_start, y_start - length - 1, lineWidth, length, color);


}




